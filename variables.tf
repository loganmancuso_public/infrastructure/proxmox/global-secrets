##############################################################################
#
# Author: Logan Mancuso
# Created: 11.28.2023
#
##############################################################################

variable "dns_names" {
  description = "dns names for your environment"
  type        = list(string)
  default = [
    "localhost"
  ]
}

## Shared Secrets ##
variable "ssh" {
  description = "ssh key for remoting into proxmox nodes and instances {key_private_path, key_public}"
  sensitive   = true
  type = object({
    key_private_path = string
    key_public       = string
  })
}

variable "proxmox" {
  description = "default credentials for the proxmox node {username, password}"
  sensitive   = true
  type = object({
    username = string # proxmox nodes only work through the root user right now
    password = string
  })
}

variable "smtp" {
  description = "smtp credentials {host, port, username , password}"
  sensitive   = true
  type = object({
    host     = string
    port     = string
    username = string
    password = string
  })
}

variable "user_operations" {
  description = <<EOT
  credentials for the packer deploy operations user, 
  this needs to be generated on your primary proxmox node 
  with highlevel permissions to the proxmox api. 
  initially this workflow will be ran without an api key. 
  after the datacenter workflow is ran, come back and populate
  this api key with permissions of the operations user. 
  {username,password,api_id,api_key}"
  EOT
  sensitive   = true
  type = object({
    username = string
    password = string
    api_id   = string
    api_key  = map(string)
  })
}

variable "instance" {
  description = <<EOT
  default credentials for an instance 
  the hashed_password is generated using this command
  mkpasswd -m sha-512 -S "salt" "password"
  ssk_key is your ssh public key, the one used for remote ssh access
  {username,password,hashed_password,salt}
  EOT
  sensitive   = true
  type = object({
    username        = string
    password        = string
    hashed_password = string
    salt            = string
  })
}