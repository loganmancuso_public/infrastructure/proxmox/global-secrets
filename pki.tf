##############################################################################
#
# Author: Logan Mancuso
# Created: 11.28.2023
#
##############################################################################

resource "vault_pki_secret_backend_root_cert" "root_ca" {
  backend              = vault_mount.pki.path
  type                 = "internal"
  common_name          = "${local.cert_root.subject.common_name} Root CA"
  ttl                  = "315360000"
  format               = "pem"
  private_key_format   = "der"
  key_type             = "rsa"
  key_bits             = 4096
  exclude_cn_from_sans = true
  ou                   = local.cert_root.subject.organization
  organization         = local.cert_root.subject.organization
  country              = local.cert_root.subject.country
  locality             = local.cert_root.subject.locality
  postal_code          = local.cert_root.subject.postal_code
  province             = local.cert_root.subject.province
}