##############################################################################
#
# Author: Logan Mancuso
# Created: 11.28.2023
#
##############################################################################

# Vault Entities #
output "entity_ids" {
  description = "Entity IDs"
  value = {
    admin = vault_identity_entity.admin.id
  }
}
# Vault Groups #
output "group_ids" {
  description = "Group IDs"
  value = {
    infra  = vault_identity_group.infra.id
    app    = vault_identity_group.app.id
    shared = vault_identity_group.shared.id
  }
}

# Vault Paths #
output "paths" {
  description = "Vault Paths to secrets"
  value = {
    shared = vault_mount.shared.path
    infra  = vault_mount.infra.path
    app    = vault_mount.app.path
    pki    = vault_mount.pki.path
  }
}

## Shared Secrets ##
output "proxmox" {
  description = "path in vault to proxmox secret"
  value = {
    mount = vault_mount.shared.path
    name  = vault_kv_secret_v2.proxmox.name
  }
}

output "user_operations" {
  description = "path in vault to user_operations secret"
  value = {
    mount = vault_mount.shared.path
    name  = vault_kv_secret_v2.user_operations.name
  }
}

output "instance" {
  description = "path in vault to instance secret"
  value = {
    mount = vault_mount.shared.path
    name  = vault_kv_secret_v2.instance.name
  }
}

output "smtp" {
  description = "path in vault to smtp secret"
  value = {
    mount = vault_mount.shared.path
    name  = vault_kv_secret_v2.smtp.name
  }
}

## Infra Secrets ##
# Root Cert
output "cert_root" {
  description = "root certificate"
  value = {
    mount = vault_mount.infra.path
    name  = vault_kv_secret_v2.cert_root.name
  }
}

## PKI ##

output "pki" {
  description = "path in vault to pki"
  value = {
    mount = vault_mount.pki.path
    name  = vault_pki_secret_backend_root_cert.root_ca.key_id
    id    = vault_pki_secret_backend_root_cert.root_ca.id
  }
}