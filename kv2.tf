##############################################################################
#
# Author: Logan Mancuso
# Created: 11.28.2023
#
##############################################################################

## INFRA ##
resource "vault_kv_secret_v2" "cert_root" {
  mount               = vault_mount.infra.path
  name                = "cert_root"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode({
    cert    = local.cert_root.cert.cert_pem
    private = local.cert_root.keys.private_key_pem
    public  = local.cert_root.keys.public_key_pem
    subject = local.cert_root.subject
  })
}

## SHARED ##

# Generate Key for ssh into instances
resource "tls_private_key" "instance" {
  algorithm = "ED25519"
}
# Generate Key for accessing private resources
resource "tls_private_key" "resources" {
  algorithm = "ED25519"
}

resource "local_sensitive_file" "instance_sshkey" {
  # this assumes that the first 2 folders are the users current home directory
  filename        = "/${split("/", path.cwd)[1]}/${split("/", path.cwd)[2]}/.ssh/instance_key"
  content         = tls_private_key.instance.private_key_openssh
  file_permission = "0600" # private keys should be 600 .pub are 644
}

resource "vault_kv_secret_v2" "proxmox" {
  mount               = vault_mount.shared.path
  name                = "proxmox"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode(merge(var.proxmox, {
    # for accessing the proxmox host
    sshkey_path   = var.ssh.key_private_path
    sshkey_public = var.ssh.key_public
    # for nodes to access hosted instances
    sshkey_instance_private = trimspace(tls_private_key.instance.private_key_openssh)
    sshkey_instance_public  = trimspace(tls_private_key.instance.public_key_openssh)
    # for the node to access private resources
    sshkey_resource_private = trimspace(tls_private_key.resources.private_key_openssh)
    sshkey_resource_public  = trimspace(tls_private_key.resources.public_key_openssh)
  }))
}

resource "vault_kv_secret_v2" "user_operations" {
  mount               = vault_mount.shared.path
  name                = "user_operations"
  cas                 = 1
  delete_all_versions = true
  data_json           = jsonencode(var.user_operations)
}

resource "vault_kv_secret_v2" "instance" {
  mount               = vault_mount.shared.path
  name                = "instance"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode(merge(var.instance, {
    # for accessing the instance
    sshkey_private = trimspace(tls_private_key.instance.private_key_openssh)
    sshkey_public  = trimspace(tls_private_key.instance.public_key_openssh)
    # for the instance to access private resources
    sshkey_resource_private = trimspace(tls_private_key.resources.private_key_openssh)
    sshkey_resource_public  = trimspace(tls_private_key.resources.public_key_openssh)
  }))
}

resource "vault_kv_secret_v2" "smtp" {
  mount               = vault_mount.shared.path
  name                = "smtp"
  cas                 = 1
  delete_all_versions = true
  data_json           = jsonencode(var.smtp)
}