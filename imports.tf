##############################################################################
#
# Author: Logan Mancuso
# Created: 11.28.2023
#
##############################################################################

data "terraform_remote_state" "hashicorp_vault" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/hashicorp-vault"
  }
}

locals {
  # hashicorp_vault
  cert_root = data.terraform_remote_state.hashicorp_vault.outputs.cert_root
}
